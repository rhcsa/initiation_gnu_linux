# Initiation aux systemes GNU_Linux  :penguin: 

___

Nous proposons quelques notes sur les bases du systeme GNU/Linux basé Red Hat (Fedora).
Il s'agit de notes redigées pendant la preparation d'une certification Red Hat (RHCSA). 

Elles manquent certainement de precisions et d'exhaustivité mais il s'agit de notes. 
Toutes suggestions de presentation, de syntaxe, de corrections techniques du contenu, etc ...  sont les bienvenues.

En aucun cas ce gitlab ni son contenu ne sont en rapport direct avec Red Hat il s'agit de notes personnelles.

Enjoy.

___
